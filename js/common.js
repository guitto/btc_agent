$(document).ready(function(){
  /* IE9FLEX対応*/
  var userAgent = window.navigator.userAgent.toLowerCase();
  if( userAgent.match(/(msie|MSIE)/)) {
    var isIE = true;
    var ieVersion = userAgent.match(/((msie|MSIE)\s|rv:)([\d\.]+)/)[3];
    ieVersion = parseInt(ieVersion);
    if(ieVersion<10){
      $(function(){
        flexibility(document.documentElement);
      });
    }
  }
});

/*　アンカーリンクアニメーション（同一ページ） */
$('a[href^="#"]').click(function(){
  var speed = 500;
  var href= $(this).attr("href");
  var target = $(href == "#" || href == "" ? 'html' : href);
  var position = target.offset().top-94;
  $("html, body").animate({scrollTop:position}, speed, "swing");
  return false;
});

/* gotopボタン */
$('.gotop').click(function(){
  var speed = 500;

  $("html, body").animate({scrollTop:0}, speed, "swing");
  return false;
});

$(window).scroll(function(){
  var scrollValue = $(window).scrollTop();
  if(scrollValue > 500){
    $('.gotop').addClass('visible');
    $('.conversion').removeClass('hide');
  } else {
    $('.gotop').removeClass('visible');
    $('.conversion').addClass('hide');
  }
});


/*　ボタンhoverアニメーション　*/
$('.button').hover(function() {
$(this).find('.ja').css('transform','scale(1.1)');
},
function () {
$(this).find('.ja').css('transform','scale(1)');
}
);

$(document).ready(function(){
  var offset = 300;
  /* スクロールアニメーションライブラリ用記述 */
  AOS.init({offset: offset,easing: 'ease-in-sine',disable: 'mobile',once: 'true'});
});

if (!window.afblpcvLpConf) {
  window.afblpcvLpConf = [];
}
window.afblpcvLpConf.push({
  siteId: "4d15665c"
});
